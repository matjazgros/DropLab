var CustomEvent = require('./custom_event_polyfill');
var Hook = require('./hook');

var HookInput = function(trigger, list) {
  Hook.call(this, trigger, list);
  this.type = 'input';
  this.event = 'input';
  this.addEvents();
};

Object.assign(HookInput.prototype, {
  addEvents: function(){
    var self = this;
    this.trigger.addEventListener('mousedown', function(e){
      var mouseEvent = new CustomEvent('mousedown.dl', {
        detail: {
          hook: self,
          text: e.target.value,
        },
        bubbles: true,
        cancelable: true
      });
      e.target.dispatchEvent(mouseEvent);
    });

    this.trigger.addEventListener('input', function(e){
      var inputEvent = new CustomEvent('input.dl', {
        detail: {
          hook: self,
          text: e.target.value,
        },
        bubbles: true,
        cancelable: true
      });
      e.target.dispatchEvent(inputEvent);
      self.list.show();
    });

    this.trigger.addEventListener('keyup', function(e){
      keyEvent(e, 'keyup.dl');
    });

    this.trigger.addEventListener('keydown', function(e){
      keyEvent(e, 'keydown.dl');
    });

    function keyEvent(e, keyEventName){
      var keyEvent = new CustomEvent(keyEventName, {
        detail: {
          hook: self,
          text: e.target.value,
          which: e.which,
          key: e.key,
        },
        bubbles: true,
        cancelable: true
      });
      e.target.dispatchEvent(keyEvent);
      self.list.show();
    }
  },
});

module.exports = HookInput;
