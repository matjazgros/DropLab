var DropDown = require('./dropdown');

var Hook = function(trigger, list){
  this.trigger = trigger;
  this.list = new DropDown(list);
  this.type = 'Hook';
  this.event = 'click';
  this.id = trigger.dataset.id;
};

Object.assign(Hook.prototype, {
  addEvents: function(){},

  constructor: Hook,
});

module.exports = Hook;
